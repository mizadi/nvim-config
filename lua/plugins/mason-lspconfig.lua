return {
  'williamboman/mason-lspconfig.nvim',
  dependencies = {
    'williamboman/mason.nvim',
    'smjonas/inc-rename.nvim',
  },
  config = function()
    local justEnsureInstalled = 0
    local servers = {
      clangd = {},
      gopls = {},
      rust_analyzer = justEnsureInstalled,
      ts_ls = {},
      lua_ls = {
        Lua = {
          workspace = {
            checkThirdParty = false,
            library = {
              vim.env.VIMRUNTIME,
            },
          },
          telemetry = { enable = false },
        },
      },
      texlab = {},
      yamlls = {},
      bashls = {},
    }

    local capabilities = vim.lsp.protocol.make_client_capabilities()
    capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

    require('mason-lspconfig').setup({
      ensure_installed = vim.tbl_keys(servers),
      handlers = {
        function(server_name)
          if servers[server_name] ~= justEnsureInstalled then
            require('lspconfig')[server_name].setup({
              capabilities = capabilities,
              on_attach = require('plugins_common').lsp_on_attach,
              settings = servers[server_name],
              filetypes = (servers[server_name] or {}).filetypes,
            })
          end
        end,
      },
    })
  end,
}

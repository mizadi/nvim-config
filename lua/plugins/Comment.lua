return {
  'numToStr/Comment.nvim',
  opts = {
    toggler = {
      line = '<leader>cc',
      block = '<leader>cb',
    },
    opleader = {
      line = '<leader>c',
      block = '<leader>b',
    },
    extra = {
      above = '<leader>cO',
      below = '<leader>co',
      eol = '<leader>cA',
    },
  },
  lazy = false,
}

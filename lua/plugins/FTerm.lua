return {
  'numToStr/FTerm.nvim',
  config = function()
    local map = vim.keymap.set
    map('n', '<leader>t', function() require('FTerm').toggle() end, {
      desc = 'Toggle floating terminal window' })
    map('t', '<C-\\><C-t>', function() require('FTerm').toggle() end, {
      desc = 'Toggle floating terminal window' })
  end,
}

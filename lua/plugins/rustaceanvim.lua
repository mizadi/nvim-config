return {
  'mrcjkb/rustaceanvim',
  version = '^5', -- Recommended
  lazy = false,   -- This plugin is already lazy
  dependencies = {
    {
      'lvimuser/lsp-inlayhints.nvim',
      opts = {}
    },
  },
  ft = { 'rust' },
  config = function()
    local g = vim.g
    local map = vim.keymap.set
    local inlayhints = require('lsp-inlayhints')
    g.rustaceanvim = {
      inlay_hints = {
        highlight = 'NonText',
      },
      tools = {
        hover_actions = {
          auto_focus = true,
        },
      },
      server = {
        on_attach = function(client, bufnr)
          inlayhints.on_attach(client, bufnr)
          require('plugins_common').lsp_on_attach(client, bufnr)
        end
      }
    }
    map('n', '<leader>mh', inlayhints.toggle, { desc = 'Toggle inlay hints' })
  end
}

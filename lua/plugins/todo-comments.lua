return {
  "folke/todo-comments.nvim",
  dependencies = { "nvim-lua/plenary.nvim" },
  config = function()
    require("todo-comments").setup({
      signs = false,
      highlight = { pattern = [[.*<((KEYWORDS)%(\(.{-1,}\))?):]], },
      search = { pattern = [[\b(KEYWORDS)(\([^\)]*\))?:]] },
    })

    local map = vim.keymap.set
    map("n", "]t", function()
      require("todo-comments").jump_next()
    end, { desc = "Next todo-like comment" })

    map("n", "[t", function()
      require("todo-comments").jump_prev()
    end, { desc = "Previous todo-like comment" })
  end,
}

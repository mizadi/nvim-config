return {
  'nvim-treesitter/nvim-treesitter',
  build = ':TSUpdate',
  config = function()
    -- Defer setup to after first render to improve startup time of
    -- 'nvim {filename}'.
    vim.defer_fn(function()
      require('nvim-treesitter.configs').setup {
        -- A list of parser names, or 'all' (the five listed parsers should
        -- always be installed)
        ensure_installed = {
          'c', 'lua', 'vim', 'vimdoc', 'query', 'rust', 'go', 'c_sharp',
          'javascript', 'typescript', 'toml', 'bash',
        },

        -- Install parsers synchronously (only applied to `ensure_installed`)
        sync_install = false,

        -- Automatically install missing parsers when entering buffer
        -- Recommendation: set to false if you don't have `tree-sitter` CLI
        -- installed locally
        auto_install = true,

        highlight = {
          enable = true,

          -- Setting this to true will run `:h syntax` and tree-sitter at the
          -- same time. Set this to `true` if you depend on 'syntax' being
          -- enabled (like for indentation). Using this option may slow down
          -- your editor, and you may see some duplicate highlights. Instead
          -- of true it can also be a list of languages
          additional_vim_regex_highlighting = false,
        },

        indent = { enable = true },
      }

      -- Treesitter folding
      if not vim.opt.diff:get() then
        vim.wo.foldmethod = 'expr'
      end
      vim.wo.foldexpr = 'nvim_treesitter#foldexpr()'
    end, 0)
  end,
}

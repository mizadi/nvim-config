return {
  'hrsh7th/vim-vsnip',
  config = function()
    local map = vim.keymap.set

    map({ 'i', 's' }, '<tab>', function()
      if vim.fn["vsnip#jumpable"](1) == 1 then
        return '<plug>(vsnip-jump-next)'
      else
        return "<tab>"
      end
    end, { expr = true })

    map({ 'i', 's' }, '<S-tab>', function()
      if vim.fn["vsnip#jumpable"](-1) == 1 then
        return '<plug>(vsnip-jump-prev)'
      else
        return "<tab>"
      end
    end, { expr = true })
  end,
}

return {
  'lcheylus/overlength.nvim',
  config = function()
    require('overlength').setup({
      highlight_to_eol = false,
    })

    local map = vim.keymap.set
    map('n', '<leader>mo', vim.cmd.OverlengthToggle, {
      desc = 'Toggle overlength marker' })
  end,
}

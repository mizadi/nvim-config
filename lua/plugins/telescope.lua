return {
  'nvim-telescope/telescope.nvim',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope-ui-select.nvim',
  },
  config = function()
    local telescope = require('telescope')
    local themes = require('telescope.themes')
    telescope.setup({
      defaults = { path_display = { 'filename_first', 'truncate' } },
      extensions = {
        ['ui-select'] = {
          themes.get_dropdown()
        }
      }
    })

    telescope.load_extension('ui-select')

    local builtin = require('telescope.builtin')
    local map = vim.keymap.set
    map('n', '<leader>ff', builtin.find_files, {
      desc = 'Find in files' })
    map('n', '<leader>fG', builtin.git_files, {
      desc = 'Find in git files' })
    map('n', '<leader>fg', builtin.live_grep, {
      desc = 'Find in files with live grep' })
    map('n', '<leader>fb', builtin.buffers, {
      desc = 'Find in buffers' })
    map('n', '<leader>fo', builtin.oldfiles, {
      desc = 'Find in old files' })
    map('n', '<leader>fh', builtin.help_tags, {
      desc = 'Find in help tags' })
    map('n', '<leader>f/', function()
      -- You can pass additional configuration to telescope to change theme,
      -- layout, etc.
      local dropdown = themes.get_dropdown({
        winblend = 10,
        previewer = false,
      })
      builtin.current_buffer_fuzzy_find(dropdown)
    end, { desc = '/ Find in current buffer' })
    map('n', '<leader>fD', builtin.diagnostics, {
      desc = 'Find in diagnostics' })
    map('n', '<leader>fd', builtin.lsp_definitions, {
      desc = 'Find in LSP definitions' })
    map('n', '<leader>ft', builtin.lsp_type_definitions, {
      desc = 'Find in LSP type definitions' })
    map('n', '<leader>fr', builtin.lsp_references, {
      desc = 'Find in LSP references' })
    map('n', '<leader>fi', builtin.lsp_implementations, {
      desc = 'Find in LSP implementations' })
    map('n', '<leader>fR', builtin.resume, { desc = 'Resume find' })
  end,
}

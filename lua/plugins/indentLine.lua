return {
  'Yggdroot/indentLine',
  config = function()
    local g = vim.g
    local map = vim.keymap.set
    local cmd = vim.cmd
    g.indentLine_enabled = false
    map('n', '<leader>mw', function()
      cmd('set list!')
      cmd('IndentLinesToggle')
    end, { desc = 'Toggle whitespace markers' })
  end,
}

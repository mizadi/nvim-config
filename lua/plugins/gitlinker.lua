return {
  'linrongbin16/gitlinker.nvim',
  cmd = 'GitLink',
  opts = {},
  keys = {
    { '<leader>gy', '<cmd>GitLink<cr>',  mode = { 'n', 'v' }, desc = 'Yank git link' },
    { '<leader>go', '<cmd>GitLink!<cr>', mode = { 'n', 'v' }, desc = 'Open git link' },
    { '<leader>gb', '<cmd>GitLink!current_branch<cr>', mode = { 'n', 'v' }, desc = 'Yank git link at current branch' },
    { '<leader>gm', '<cmd>GitLink!default_branch<cr>', mode = { 'n', 'v' }, desc = 'Yank git link at default branch' },
  },
}

return {
  'RRethy/vim-illuminate',
  config = function()
    local illuminate = require('illuminate')

    illuminate.configure()

    illuminate.pause()

    local set_hl = vim.api.nvim_set_hl
    local map = vim.keymap.set

    -- change the highlight style
    set_hl(0, "IlluminatedWordText", { link = "Search" })
    set_hl(0, "IlluminatedWordRead", { link = "Search" })
    set_hl(0, "IlluminatedWordWrite", { link = "Search" })

    --- auto update the highlight style on colorscheme change
    vim.api.nvim_create_autocmd({ "ColorScheme" }, {
      pattern = { "*" },
      callback = function()
        set_hl(0, "IlluminatedWordText", { link = "Search" })
        set_hl(0, "IlluminatedWordRead", { link = "Search" })
        set_hl(0, "IlluminatedWordWrite", { link = "Search" })
      end
    })

    map('n', '<leader>mi', function ()
      illuminate.toggle()
    end, { desc = 'Toggle variable illumination' })
  end
}

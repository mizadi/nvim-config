return {
  'bufbuild/vim-buf',
  dependencies = {
    'dense-analysis/ale',
  },
  config = function()
    vim.g.ale_linters = { proto = { 'buf-lint' } }
    vim.g.ale_lint_on_text_changed = 'never'
    vim.g.ale_linters_explicit = 1
  end
}

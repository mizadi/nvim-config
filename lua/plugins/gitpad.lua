return {
  'yujinyuz/gitpad.nvim',
  config = function()
    require('gitpad').setup({
      dir = '~/.gitpad',
      on_attach = function(bufnr)
        vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>q', '<Cmd>wq<CR>', { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>w', '<Cmd>w<CR>', { noremap = true, silent = true })
        vim.bo.textwidth = 0 -- Prevent auto-wrapping
      end,
    })
  end,
  keys = {
    {
      '<leader>np',
      function()
        require('gitpad').toggle_gitpad({
          title = 'Project notes',
        })
      end,
      desc = 'gitpad project',
    },
    {
      '<leader>nb',
      function()
        require('gitpad').toggle_gitpad_branch({
          title = 'Branch notes',
        })
      end,
      desc = 'gitpad branch',
    },
    {
      '<leader>nvs',
      function()
        require('gitpad').toggle_gitpad_branch({
          window_type = 'split',
          split_win_opts = { split = 'right' },
          title = 'Branch notes',
        })
      end,
      desc = 'gitpad branch vertical split',
    },

    -- Daily notes
    {
      '<leader>nd',
      function()
        local date_filename = 'daily-' .. os.date('%Y-%m-%d.md')
        require('gitpad').toggle_gitpad({
          filename = date_filename,
          title = 'Daily notes',
        })
      end,
      desc = 'gitpad daily notes',
    },

    -- Per file notes
    {
      '<leader>nf',
      function()
        local filename = vim.fn.expand('%:p') -- or just use vim.fn.bufname()
        if filename == '' then
          vim.notify('empty bufname')
          return
        end
        filename = vim.fn.pathshorten(filename, 2) .. '.md'
        require('gitpad').toggle_gitpad({
          filename = filename,
          title = 'Current file notes',
        })
      end,
      desc = 'gitpad per file notes',
    },
  },
}

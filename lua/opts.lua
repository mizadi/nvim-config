local opt = vim.opt
local cmd = vim.cmd
local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

opt.number = true
opt.relativenumber = true
opt.scrolloff = 2
opt.signcolumn = 'yes'

opt.encoding = 'utf8'
opt.fileencoding = 'utf8'

opt.syntax = 'on'
opt.termguicolors = os.getenv('STY') == nil

opt.ignorecase = true
opt.smartcase = true
opt.incsearch = true
opt.hlsearch = true

opt.expandtab = true
opt.shiftwidth = 2
opt.softtabstop = 2
opt.tabstop = 2
opt.smartindent = true

opt.splitbelow = true
opt.splitright = true

opt.mouse = 'a'
opt.cursorline = true
opt.foldcolumn = '4'
opt.wrap = false
opt.textwidth = 80

opt.swapfile = false
opt.backup = false
opt.undodir = os.getenv('HOME') .. '/.vim/undodir'
opt.undofile = true

opt.updatetime = 300

opt.listchars = {
  tab = '▸ ',
  eol = '¬',
  trail = '␣',
}

cmd.colorscheme('slate')
cmd.highlight('MatchParen guibg=black guifg=#ffd700')

-- Highlight momentarily on yank
local highlight_group = augroup('YankHighlight', { clear = true })
autocmd('TextYankPost', {
  callback = function() vim.highlight.on_yank() end,
  group = highlight_group,
  pattern = '*',
})

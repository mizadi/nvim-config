local g = vim.g

g.background = 'dark'
g.mapleader = ','
g.localleader = ','

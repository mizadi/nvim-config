local map = vim.keymap.set
local cmd = vim.cmd

map({ 'i', 's' }, 'jk', '<esc>', { desc = 'Exit to normal mode' })
map('n', 'j', 'gj')
map('n', 'k', 'gk')
map('n', '<leader><space>', cmd.noh, { desc = 'Hide search highlights' })
map('v', 'J', ":m '>+1<cr>gv=gv")
map('v', 'K', ":m '<-2<cr>gv=gv")
-- map('n', '<C-d>', '<C-d>zz')
-- map('n', '<C-u>', '<C-u>zz')
map('n', 'n', 'nzzzv')
map('n', 'N', 'Nzzzv')
map('n', '<leader>mw', function() cmd('set list!') end, {
  desc = 'Toggle whitespace markers' })
